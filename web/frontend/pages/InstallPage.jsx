import {Card, Page, Layout, TextContainer, Text, SettingToggle} from "@shopify/polaris";
import { TitleBar } from "@shopify/app-bridge-react";
import { useTranslation } from "react-i18next";

export default function InstallPage() {
  const { t } = useTranslation();
  function handleAction(){}
  return (
    <Page>
      <TitleBar
        title={t("InstallPage.title")}
      />
      <Layout>
          <Layout.AnnotatedSection
          title={t("InstallPage.title")}
          description={'PageDescription'}>
              <SettingToggle
              action={{
                  content:"Install",
                  onAction: handleAction
              }}
              enabled={true}>
                  The banner script is <Text as={'strong'}>unistaled</Text>
              </SettingToggle>

          </Layout.AnnotatedSection>
        <Layout.Section>

        </Layout.Section>
      </Layout>
    </Page>
  );
}
