import {ResourcePicker} from "@shopify/app-bridge-react";
import {useEffect, useState} from "react";
import ProductPage from "../components/ProductPage.jsx";
import ProductEmptyState from "../components/ProductEmptyState.jsx";

export default function HomePage() {
    const [isOpen, setIsOpen] = useState(false);
    const [products, setProducts] = useState([]);
    const [productsId, setProductsId] = useState([]);
    useEffect(() => {
        const ids = products.map(product => {
            return {
                id: product.id
            }
        });
        setProductsId(ids);
    }, [products]);

    function handleProductSection(payload) {
        setIsOpen(false);
        setProducts(payload.selection);
    }

    return (
        <>
            <ResourcePicker
                resourceType="Product"
                open={isOpen}
                onCancel={() => setIsOpen(false)}
                onSelection={handleProductSection}
                initialSelectionIds={productsId}
            />
            {products.length > 0 ?
                <ProductPage setIsOpen={setIsOpen} products={products} />
                :
                <ProductEmptyState setIsOpen={setIsOpen} />
            }
        </>

    );
}
