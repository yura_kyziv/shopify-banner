import React from "react";
import {LegacyStack, ResourceList, Text, Thumbnail} from "@shopify/polaris";
import {HideMinor} from '@shopify/polaris-icons';

export default function ProductItem({product}) {
    const image = product.images[0] ? product.images[0]?.originalSrc : HideMinor;
    const media = <Thumbnail source={image} alt={product.title}/>;
    const price = product.variants[0].price;
    const title = product.title;
    return (
        <ResourceList.Item
            media={media}
            id={product.id}
            accessibilityLabel={`View details for ${title}`}
        >
            <LegacyStack>
                <LegacyStack.Item fill>
                    <Text as='strong'>{title}</Text>
                </LegacyStack.Item>
                <LegacyStack.Item>
                    <Text as='p'>{price}</Text>
                </LegacyStack.Item>
            </LegacyStack>
        </ResourceList.Item>
    );
}