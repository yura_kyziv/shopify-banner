import React from "react";
import {LegacyCard, ResourceList} from "@shopify/polaris";
import ProductItem from "./ProductItem.jsx";

export default function ProductList({products}) {
    return (
        <LegacyCard>
            <ResourceList
                showHeader
                resourceName={{singular:"Product", plural:"Products"}}
                items={products}
                renderItem={product => {
                    return <ProductItem product={product}/>
                }}
            />
        </LegacyCard>
    )
}