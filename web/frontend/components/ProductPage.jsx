import React from "react";
import ProductList from "./ProductList.jsx";
import {Page} from "@shopify/polaris";
import {useTranslation} from "react-i18next";

export default function ProductPage ({setIsOpen, products}) {
    const {t} = useTranslation();
    return (
        <Page
            primaryAction={{
                content: 'Select product',
                onAction: () => setIsOpen(true)
            }}
            title={t("HomePage.title")}
        >
            <ProductList products={products}/>
        </Page>
    )
}